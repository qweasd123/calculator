var numbers = document.querySelectorAll('.number'),
  operations = document.querySelectorAll('.operation'),
  decimalbtn = document.getElementById('decimal'),
  clearBtns = document.querySelectorAll('.clearBtn'),
  resultBtn = document.getElementById('equal'),
  howWorkBtn = document.getElementById('howWorkBtn'),
  display = document.getElementById('display'),
  MemoryCurrentNumber = 0;
  MemoryNewNumber = false,
  MemoryPendingOperation =  '';


for (var i=0; i<numbers.length; i++) {
  var number = numbers[i];
  number.addEventListener('click', function (e) {
    numberPress(e.target.textContent);
  });
};

for (var i=0; i<operations.length; i++) {
  var operationBtn = operations[i];
  operationBtn.addEventListener('click', function (e) {
      operation(e.target.textContent);
  });
};

for (var i=0; i<clearBtns.length; i++) {
  var clearBtn = clearBtns[i];
  clearBtn.addEventListener('click', function (e) {
    clear(e.srcElement.id);
  });
};

decimalbtn.addEventListener('click', decima);

resultBtn.addEventListener('click', result);

howWorkBtn.addEventListener('click', howWork);

function numberPress(number) {
  if (MemoryNewNumber){
    display.value = number;
    MemoryNewNumber = false;
  }
  else {
    if (display.value === '0') {
      display.value = number;
    }
    else {
      display.value += number;
    };
  }



};

function operation(op) {
 var localOperationMemory = display.value;
 if (MemoryNewNumber && MemoryPendingOperation !== '='){
   display.value = MemoryCurrentNumber;
   console.log("MemoryNewNumber = true");
   console.log(MemoryNewNumber);
 }
 else {
   MemoryNewNumber = true;

   switch (MemoryPendingOperation) {
     case '+':
       MemoryCurrentNumber += parseFloat(localOperationMemory);
       console.log(MemoryPendingOperation);
       console.log("+ " + MemoryCurrentNumber);
       break;
     case '-':
       MemoryCurrentNumber -= parseFloat(localOperationMemory);
       console.log(MemoryPendingOperation);
       console.log("- " + MemoryCurrentNumber);
       break;
     case '/':
       MemoryCurrentNumber /= parseFloat(localOperationMemory);
       console.log(MemoryPendingOperation);
       console.log("/ " + MemoryCurrentNumber);
       break;
     case '*':
       MemoryCurrentNumber *= parseFloat(localOperationMemory);
       console.log(MemoryPendingOperation);
       console.log("* " + MemoryCurrentNumber);
       break;
     default:
       MemoryCurrentNumber = parseFloat(localOperationMemory);
       console.log(MemoryPendingOperation);
       console.log("default " + MemoryCurrentNumber);
   }
   display.value = MemoryCurrentNumber;

 };
  MemoryPendingOperation =op;

};

function decima() {
  var localDecimaMemory = display.value;
  if (MemoryNewNumber){
    localDecimaMemory = '0.';
    MemoryNewNumber=false;
  }
  else {
    if (localDecimaMemory.indexOf('.') ===-1){
      localDecimaMemory += '.';
    }
  };
  display.value = localDecimaMemory;
  //console.log('Клик на decima');
};

function clear(id) {
  if (id === 'ce'){
    display.value = '0';
    MemoryNewNumber = true;
  }
  else if (id === 'c') {
    display.value = '0';
    MemoryCurrentNumber =0;
    MemoryNewNumber = true;
    MemoryPendingOperation = '';
  };
};


function howWork() {
  console.log('Клик на howWork');

};

